<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex3Tests extends WebTestCaseExtended {

    function defaultPageIsForm() {
        $this->get(BASE_URL);

        $this->assertField('message');
    }

    function formSubmissionRedirects() {
        $this->get(BASE_URL);

        $this->setMaximumRedirects(0);

        $this->clickSubmitByName('button');

        $this->assertResponse([302]);
    }

    function redirectUrlIsCorrect() {
        $this->get(BASE_URL);

        $this->setFieldByName('message', "hello, world");

        $this->clickSubmitByName('button');

        $this->assertEqual($this->getQueryString(), '?abc=hello%2C+world');
    }

    function correctMessageIsDisplayed() {
        $this->get(BASE_URL);

        $this->setFieldByName('message', "123");

        $this->clickSubmitByName('button');

        $this->assertText("321");
    }
}

(new Ex3Tests())->run(new PointsReporter(10,
    [2 => 2,
     3 => 6,
     4 => 10]));
